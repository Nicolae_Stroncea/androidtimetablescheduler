package com.stroncea.androidtimetablescheduler;

/**
 * Interface to specify what happens when you swipe in different directions on the layout
 */
public interface SwipeGestureCallBack {
    void onSwipe(Direction direction);
}