package com.stroncea.androidtimetablescheduler;


public interface Scorable {
    /**
     *
     * @return the score
     */
    double giveScore();
}
