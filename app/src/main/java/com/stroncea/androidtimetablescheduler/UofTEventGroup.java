package com.stroncea.androidtimetablescheduler;

import java.util.List;
import java.util.Map;

/**
 * Represents a UofTEventGroup.
 */
public class UofTEventGroup extends EventGroup<UofTEvent>{
    private String instructor;


    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }





}
